﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Scrypt;

namespace Scrypt_Tester
{
    class Program
    {
        public static void Main(string[] args)
        {
            String choice;

            do
            {
                Console.WriteLine("1. Hasher");
                Console.WriteLine("2. Comparer");
                Console.WriteLine("3. Login Demo");
                Console.Write("Enter your choice: ");
                choice = Console.ReadLine();
                Console.WriteLine(" ");

                if (choice.Equals("1"))
                {
                    Console.Write("Enter Password: ");
                    String plainpass = Console.ReadLine();


                    String cipherpass = HashObj.Hash(plainpass);

                    Console.Write("The hashed password is: " + cipherpass);
                    Console.WriteLine(" ");
                }
                else if (choice.Equals("2"))
                {
                    Console.Write("Enter Plaintext: ");
                    String plainpass = Console.ReadLine();

                    Console.Write("Enter Hash: ");
                    String hashpass = Console.ReadLine();


                    //Boolean result = HashObj.Compare(plainpass, hashpass);
                    bool result = HashObj.Compare(hashpass, plainpass);

                    if (result == true)
                    {
                        Console.WriteLine("Password and Hash matches");
                        Console.WriteLine(" ");
                    }
                    else if (result == false)
                    {
                        Console.WriteLine("Password and Hash does not match");
                        Console.WriteLine(" ");
                    }
                }
                else if (choice.Equals("3"))
                {
                    String subchoice;
                    String dbUsername = "";
                    String dbPassword = "";

                    do
                    {
                        Console.WriteLine("Login Demo Options");
                        Console.WriteLine("1. Create User");
                        Console.WriteLine("2. Login");
                        Console.WriteLine("0. Exit");
                        Console.Write("Enter your choice: ");
                        subchoice = Console.ReadLine();
                        Console.WriteLine(" ");

                        if (subchoice.Equals("1"))
                        {
                            Console.WriteLine("Create a user account to get started!");
                            Console.Write("Enter username: ");
                            String username = Console.ReadLine();
                            Console.Write("Enter password: ");
                            String password = Console.ReadLine();
                            Console.WriteLine(" ");

                            //Create hash of password
                            String hashpw = HashObj.Hash(password);

                            SqlConnection myConnection = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["localdbConnectionString1"].ConnectionString);
                            String query = "INSERT INTO [dbo].[User] (Username, Password) VALUES (@username, @password)";
                            SqlCommand myCommand = new SqlCommand(query, myConnection);

                            myCommand.Parameters.AddWithValue("username", username);
                            myCommand.Parameters.AddWithValue("password", hashpw);

                            myConnection.Open();
                            myCommand.ExecuteNonQuery();
                            myConnection.Close();

                            Console.WriteLine("Your account has been created!");
                            Console.WriteLine(" ");
                        }
                        else if (subchoice.Equals("2"))
                        {
                            Console.WriteLine("Welcome to Acme Internal Database!");
                            Console.Write("Enter username: ");
                            String username = Console.ReadLine();
                            Console.Write("Enter password: ");
                            String password = Console.ReadLine();
                            Console.WriteLine(" ");

                            //hash the password
                            String hashpw = HashObj.Hash(password);

                            SqlConnection myConnection = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["localdbConnectionString1"].ConnectionString);
                            String query = "SELECT * FROM [dbo].[User] WHERE [Username] = @username";
                            SqlCommand myCommand = new SqlCommand(query, myConnection);

                            myCommand.Parameters.AddWithValue("username", username);

                            myConnection.Open();
                            SqlDataReader reader = myCommand.ExecuteReader();

                            if (reader.Read())
                            {
                                dbUsername = reader["Username"].ToString();
                                dbPassword = reader["Password"].ToString();
                            }

                            if (dbUsername.Equals("") || dbPassword.Equals(""))
                            {
                                Console.WriteLine("This account does not exist.");
                                Console.WriteLine("Please create an account first");
                                Console.WriteLine(" ");
                            }
                            else
                            {
                                bool pwcheck = HashObj.Compare(dbPassword, password);

                                if (username.Equals(dbUsername) && pwcheck.Equals(true))
                                {
                                    Console.WriteLine("You have successfully logged into the database!");
                                    Console.WriteLine("Welcome " + dbUsername + "!");
                                    Console.WriteLine(" ");
                                }
                                else
                                {
                                    Console.WriteLine("Identity Verification Failed!");
                                    Console.WriteLine("This failed login will be reported to the administrator.");
                                    Console.WriteLine(" ");
                                }
                            }
                            myConnection.Close();
                        }
                    } while (!subchoice.Equals("0"));

                }
            } while (!choice.Equals("0"));
        }
    }
}
