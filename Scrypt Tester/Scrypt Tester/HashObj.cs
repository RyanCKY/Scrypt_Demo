﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Scrypt;

namespace Scrypt_Tester
{
    class HashObj
    {
        public static string Hash (String plaintext)
        {
            ScryptEncoder encoder = new ScryptEncoder();
            String hashedText = encoder.Encode(plaintext);
            return hashedText;
        }

        public static bool Compare (String hashedText, String plainText)
        {
            ScryptEncoder encoder = new ScryptEncoder();
            //Boolean result = encoder.Compare(hashedText, plainText);
            Boolean result = encoder.Compare(plainText, hashedText);
            return result;
        }
    }
}
